# ![alt text](doc/biscuit_logo.png "logo") The Biscuit Language

## About
The Biscuit is programming language inspired by C and Rust.

## Change log
* 0.3.0 - syntax changes
* 0.2.0 - pre alpha basic language with lot of bugs :)

## Platforms
* MacOS (clang)
* Linux (clang/gcc)
* Windows (experimental Cygwin)

## Language basics
[here](https://github.com/travisdoor/bl/blob/master/doc/readme.md "here")

## Compiler params
```
-ast-dump     Print out ast.
-lex-dump     Print tokens.
-syntax-only  Check syntax only. 
-emit-llvm    Write bytecode to disk.
-run          Run during compilation.
```
