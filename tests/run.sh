#!/bin/bash
blc -run -no-bin src/constants.bl
blc -run -no-bin src/enums.bl
blc -run -no-bin src/globals.bl
blc -run -no-bin src/ifs.bl
blc -run -no-bin src/lambdas.bl
blc -run -no-bin src/localfn.bl
blc -run -no-bin src/loops.bl
blc -run -no-bin src/pointers.bl
blc -run -no-bin src/structs.bl
